The Produce & Publish Client Connector for Plone
================================================

Documentation
-------------

See http://packages.python.org/zopyx.smartprintng.plone

Source code
-----------
See https://bitbucket.org/ajung//zopyx.smartprintng.plone

Bugtracker
----------
See  https://bigbucket.org/ajung/zopyx.smartprintng.plone/issues

Licence
-------
Published under the GNU Public Licence Version 2 (GPL 2)

Author
------
| ZOPYX Limited
| Hundskapfklinge 33
| D-72074 Tübingen, Germany
| info@zopyx.com
| www.zopyx.com
| www.produce-and-publish.info

